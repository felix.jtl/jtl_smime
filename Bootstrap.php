<?php declare(strict_types=1);

namespace Plugin\jtl_smime;

use JTL\Events\Dispatcher;
use JTL\Plugin\Bootstrapper;
use PHPMailer\PHPMailer\PHPMailer;

/**
 * Class Bootstrap
 * @package Plugin\jtl_smime
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @param Dispatcher $dispatcher
     */
    public function boot(Dispatcher $dispatcher): void
    {
        parent::boot($dispatcher);
        $dispatcher->listen('shop.hook.' . \HOOK_MAILER_PRE_SEND, function (array $args) {
            $config = $this->getPlugin()->getConfig();
            $cert   = $config->getValue('cert_file');
            $key    = $config->getValue('key_file');
            if ($cert === '' || $key === '') {
                return;
            }
            if (!\file_exists($cert) || !\is_readable($cert) || !\file_exists($key) || !\is_readable($key)) {
                return;
            }
            /** @var PHPMailer $mailer */
            $mailer = $args['phpmailer'];
            $mailer->sign($cert, $key, $config->getValue('key_password'), $config->getValue('chain_file'));
        });
    }
}
